using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ManualUpdater
{
    class Program
    {
        // first arg is the filepath (same as the first prompt)
        // second arg is the web address (can be localhost (I assume (at least, I don't see why not))) (same as second prompt)
        static void Main(string[] args)
        {
            string fileToUpload;
            string Location;

            if (args.Length == 2)
            {
                fileToUpload = args[0];
                Location = args[1];
            }
            else
            {
                Console.WriteLine("Enter manual update File Location (The .zip). Alternatively, link a text document with a list of manual patch file locations:");
                fileToUpload = Console.ReadLine();
                Console.WriteLine("Enter the Web Address to upload this file to. Alternatively, link a text document with a list of URLS:");
                Location = Console.ReadLine();
            }

            var SingleSite = false;
            List<string> SitesToUpdate = new List<string>();
            List<string> ManualPatches = new List<string>();


            //Check for text document or single site
            if(Location.ToLower().Contains("http://") || Location.ToLower().Contains("https://"))
            {
                if (!Location.EndsWith("/"))
                {
                    Location += "/";
                }
                SingleSite = true;
                SitesToUpdate.Add(Location);
            }
            else
            {
                SitesToUpdate.AddRange(File.ReadAllLines(Location));
            }

            //Check for text document or single file
            if(fileToUpload.ToLower().Contains(".zip"))
            {
                ManualPatches.Add(fileToUpload);
            }
            else
            {
                ManualPatches.AddRange(File.ReadAllLines(fileToUpload));
                ManualPatches.RemoveAll(y => y.StartsWith("--"));
            }

            

            if(!SingleSite)
            {
                SitesToUpdate.ForEach((item) => 
                {
                    if (!item.EndsWith("/"))
                    {
                        item += "/";
                    }
                });
            }

            List<string> ErrorSite = new List<string>();

            foreach(string File in ManualPatches)
            {
                foreach (string site in SitesToUpdate)
                {

                    if(ErrorSite.Contains(site))
                    {
                        Console.WriteLine($"Skipping \"{site}\" due to previous error occuring.");
                        continue;
                    }
                    Console.WriteLine($"Updating Site \"{site}\" with manual update {Path.GetFileNameWithoutExtension(File)}");
                    try
                    {
                        string url = site + "App/UploadManualPackage";
                        var Success = Upload(url, File);
                        if (Success)
                        {
                            Console.WriteLine("File Uploaded. Attempting to Patch");
                            SuccessfulUploadContinue(site, File);
                            Console.WriteLine("File Successfully Patched");
                        }
                    }
                    catch (Exception Ex)
                    {
                        Console.WriteLine("An Error Occured: " + Ex.Message);
                        ErrorSite.Add(site);
                    }
                    Console.WriteLine("----------------------------");

                }
            }
            Console.WriteLine("Done...");
            Console.ReadLine();
        }

        private static bool Upload(string actionUrl, string FileLocation)
        {
            HttpContent bytesContent = new ByteArrayContent(File.ReadAllBytes(FileLocation));
            using (var client = new HttpClient())
            using (var formData = new MultipartFormDataContent())
            {

                if(actionUrl.ToLower().StartsWith("https"))
                {
                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                }
                formData.Add(bytesContent, "files", $"\"{Path.GetFileName(FileLocation)}\"");
                var response = client.PostAsync(actionUrl, formData).Result;
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
            }
            return false;
        }

        private static void SuccessfulUploadContinue(string Location, string fileToUpload)
        {
            var url = Location + "API/DataAPI/PerformAction?ActionType=PerformActionOnServer&ActionName=ApplyManualUpdate&ActionData={PackageName: '" + Path.GetFileNameWithoutExtension(fileToUpload) + "'}";
            using (var client = new WebClient())
            {
                byte[] result = client.DownloadData(url);
                string responseAsString = Encoding.Default.GetString(result);
            }
        }
    }
}
